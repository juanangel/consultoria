<?php
/*
Template Name: Página de Incorporate
*/
get_header(); ?>

	<?php do_action( 'spacious_before_body_content' ); ?>

	<div id="primary">
		<div id="content" class="clearfix">
			
			<?php
			if ( isset($_GET['sent']) ){
				if ( $_GET['sent'] == '1'){
					echo "<p> ✔ Formulario enviado correctamente</p><br>";
				}
				else {
					echo "<p> Hubo un error al enviar</p><br>";
				}
			}
			?>

			<h5 align="center" style="color:#50AE69">¿Quiere formar parte de Consultoría Font?</h5>
			<h6 align="center" style="color:#4BC76D">No dude en contactarnos, le esperamos.</h6>

			<br/>

			<form method="post" action="<?php echo admin_url( 'admin-post.php' ) ?>" >
				<label for="name">Nombre:</label>
				<input type="text" name="name" id="name" required>

				<br>
				<label for="email">Correo:</label>
				<input type="email" name="email" id="email" required>

				<br>
				<label for="message">Mensaje:</label>
				<textarea name="message" id="message" cols="30" rows="10" required></textarea>

				<br>
				<p><input type="checkbox" id="terms" name="terms" required> He leído y acepto los <a href="/wordpress/condiciones-de-uso/">Términos y Condiciones</a></p>

				<br>
				<input type="hidden" name="action" value="process_form">
				<input type="submit" name="submit" value="Enviar">
			</form>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php spacious_sidebar_select(); ?>

	<?php do_action( 'spacious_after_body_content' ); ?>

<?php get_footer(); ?>
