<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'proyectos' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'H^A7~9$LGvSWWRebot#~91U>7o@7Sz~2BH7tJ?+_MFMV}w&!eSNxpJ>1`=ioE&&;' );
define( 'SECURE_AUTH_KEY',  '>9&)@8%VPxoc5E%q},=po7>&xNcAjAzGxtH^mR~R;i11|^L{_P(T `jHFx=z)?s;' );
define( 'LOGGED_IN_KEY',    'uP;yS7}bX@0kyky<mEqm(?y9{?(]^_1t[J(I7~d<iIu@(w4B^)@tXRZeK#uu<y/i' );
define( 'NONCE_KEY',        '{qYb}c<3!o6upw<rDN(]qK|IpA#TqPi<5v=72=t#NBO(-|s}_Qs8 6&h@?[%oqq]' );
define( 'AUTH_SALT',        ']/2Q*fcz=x/sanfatpqZIu*E[q6haK_VEMS)TI(i+JyHN llJ=.p:Q&JN#*FNRl;' );
define( 'SECURE_AUTH_SALT', '+U|9}MTJ]?DBqf,Go9*$[Mag+xJR>Zoff2,I%Ev*o|.T&9=EuK8cY_ <^jYu~T!6' );
define( 'LOGGED_IN_SALT',   '{nO }!ZmBT 7e%sg7|]f<aW;hmLD/r^sGN=b%.RV;GO6CP>u:3^1.HH2}3WQo5_G' );
define( 'NONCE_SALT',       '@mv }+46g[(l=(E2~4-T]pNOJ81w/JPR~*L}aD?W}]FuXz/%y}Wxr&f0~.u_PQ+e' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
